FROM lucasgirardcz/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > eog.log'

COPY eog.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode eog.64 > eog'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' eog

RUN bash ./docker.sh
RUN rm --force --recursive eog _REPO_NAME__.64 docker.sh gcc gcc.64

CMD eog
